import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
  TextInput,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

function itemRender({item}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{flexDirection: 'row', alignItems: 'center', paddingRight: 5}}>
        <View style={styles.itemList}>
          <View>
            <AntDesign
              name={item.icon}
              color={'white'}
              size={20}
              style={styles.imageItem}
            />
          </View>
          <Text numberOfLines={1} style={styles.titleItem}>
            {item.name}
          </Text>
        </View>
        <AntDesign name="right" color={'white'} size={15} />
      </TouchableOpacity>
    </View>
  );
}
export default function SettingView({navigation}) {
  const setting = [
    {name: 'Cuenta', icon: 'user'},
    {name: 'Notificaciones', icon: 'bells'},
    {name: 'Privacidad y Seguridad', icon: 'lock'},
    {name: 'Ayuda y Soporte', icon: 'customerservice'},
    {name: 'Acerca', icon: 'questioncircleo'},
  ];
  return (
    <View style={{flex: 1,backgroundColor: '#202020',}}>
      <View
        style={{
          margin: 20,
          flex: 1,
        }}>
        <View style={{paddingTop: '10%'}}>
          <FlatList
            data={setting}
            renderItem={itemRender}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemList: {
    flex: 1,
    flexDirection: 'row',

    alignItems: 'center',
    padding: 5,
    //backgroundColor: '#202020',
  },
  imageItem: {
    margin: 5,
    borderRadius: 100,
  },
  titleItem: {
    /* backgroundColor: 'blue', */
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 15,
    color: 'white',
  },
});
