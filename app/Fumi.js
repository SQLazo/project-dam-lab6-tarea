import React from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  TextInput,
  TouchableWithoutFeedback,
  View,
  StyleSheet,
  Easing,
  Text,
} from 'react-native';

import BaseInput from './BaseInput';

export default class Fumi extends BaseInput {
  static propTypes = {
    
    iconClass: PropTypes.func.isRequired,
    iconName: PropTypes.string.isRequired,
    iconColor: PropTypes.string,
    iconSize: PropTypes.number,
    passiveIconColor: PropTypes.string,
    height: PropTypes.number,
    borderColor: PropTypes.string,

  };

  static defaultProps = {
    height: 48,
    iconColor: '#00aeef',
    iconSize: 20,
    iconWidth: 40,
    inputPadding: 16,
    passiveIconColor: '#a3a3a3',
    animationDuration: 300,
    borderColor: '#363636',
  };
  
  render() {
    const {
      iconClass,
      iconColor,
      iconSize,
      passiveIconColor,
      iconName,
      label,
      style: containerStyle,
      inputStyle,
      height: inputHeight,
      inputPadding,
      iconWidth,
      labelStyle,
    } = this.props;
    const { focusedAnim, value } = this.state;
    const AnimatedIcon = Animated.createAnimatedComponent(iconClass);
    const ANIM_PATH = inputPadding + inputHeight;
    const NEGATIVE_ANIM_PATH = ANIM_PATH * -1;

    return (
      <View
        style={[styles.container, containerStyle, {
          height: ANIM_PATH,backgroundColor: this.props.borderColor,
        }]}
        onLayout={this._onLayout}
      >
        <TouchableWithoutFeedback onPress={this.focus}>
          <AnimatedIcon
            name={iconName}
            color={iconColor}
            size={iconSize}
            style={{
              position: 'absolute',
              left: inputPadding,
              bottom: focusedAnim.interpolate({
                inputRange: [0, 0.5, 0.51, 0.7, 1],
                outputRange: [
                  22,
                  ANIM_PATH,
                  NEGATIVE_ANIM_PATH,
                  NEGATIVE_ANIM_PATH,
                  22,
                ],
              }),
              color: focusedAnim.interpolate({
                inputRange: [0, 0.5, 1],
                outputRange: [passiveIconColor, iconColor, iconColor],
              }),
            }}
          />
        </TouchableWithoutFeedback>
        <View
          style={[
            styles.separator,
            {
              height: inputHeight,
              left: iconWidth + 8,
            },
          ]}
        />
        <TouchableWithoutFeedback onPress={this.focus}>
          <Animated.View
            style={{
              position: 'absolute',
              left: iconWidth + inputPadding,
              height: inputHeight,
              top: focusedAnim.interpolate({
                inputRange: [0, 0.5, 0.51, 0.7, 1],
                outputRange: [
                  21,
                  ANIM_PATH,
                  NEGATIVE_ANIM_PATH,
                  NEGATIVE_ANIM_PATH,
                  inputPadding / 2,
                ],
              }),
            }}
          >
            <Animated.Text
              style={[
                styles.label,
                {
                  fontSize: focusedAnim.interpolate({
                    inputRange: [0, 0.7, 0.71, 1],
                    outputRange: [16, 16, 12, 12],
                  }),
                  color: focusedAnim.interpolate({
                    inputRange: [0, 0.7],
                    outputRange: ['#696969', '#fff'],
                  }),
                },
                labelStyle,
              ]}
            >
              {label}
            </Animated.Text>
          </Animated.View>
        </TouchableWithoutFeedback>
        <TextInput
          ref={this.input}
          {...this.props}
          style={[
            styles.textInput,
            {
              marginLeft: iconWidth + inputPadding,
              color: iconColor,
            },
            inputStyle,
          ]}
          value={value}
          onBlur={this._onBlur}
          onFocus={this._onFocus}
          onChange={this._onChange}
          underlineColorAndroid={'transparent'}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    paddingTop: 16,
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textInput: {
    flex: 1,
    color: 'white',
    fontSize: 18,
    padding: 7,
    paddingLeft: 0,
    
  },
  separator: {
    position: 'absolute',
    width: 1,
    backgroundColor: '#8a8a8a',
    top: 8,
  },
});