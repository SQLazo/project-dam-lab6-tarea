//--------------------------------------------------------------------------------------------------------------------//
import React, { Component } from 'react';
import {
    View, 
    FlatList, 
    StyleSheet, 
    Text, 
    Image,
    TouchableOpacity
} from 'react-native';

//--------------------------------------------------------------------------------------------------------------------//
import Icon from 'react-native-vector-icons/FontAwesome';
import Ico from "react-native-vector-icons/Ionicons";
import Ic from "react-native-vector-icons/AntDesign";
import I from "react-native-vector-icons/Entypo";


//--------------------------------------------------------------------------------------------------------------------//
export default class ConexionFetch extends Component{
    constructor(props){
        super(props);

        this.state = {
            textValue: 0,
            count: 0,
            items: [],
            error: null,
            id: this.props.route.params.itemId,
        };
    }
    async componentDidMount() {
        await fetch(`https://yts.mx/api/v2/movie_details.json?movie_id=${this.state.id}`)
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        items: result.data.movie,
                    });
                },
                error => {
                    this.setState({
                        error: error,
                    });
                },
            );
    }

    render(){
        return(
            <View style={styles.container}>
                <View>
                    <Image source={{uri: this.state.items.background_image_original}} style={styles.itemBackground}
                    resizeMode="cover"/>
                    <Image source={{uri: this.state.items.medium_cover_image}} style={styles.itemImage} resizeMode="cover"/>
                </View>
                <View style={styles.top}>
                    <TouchableOpacity style={styles.like}>
                        <Ico
                            name="ios-add"
                            color="#fff"
                            size={25}
                        />
                        <Text style={styles.title}>MyList</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.like}>
                        <Ic
                            name="like1"
                            color="#fff"
                            size={25}
                        />
                        <Text style={styles.title}>Like</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.like}>
                        <Ic
                            name="sharealt"
                            color="#fff"
                            size={25}
                        />
                        <Text style={styles.title}>Share</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.text}>
                    <TouchableOpacity style={styles.button}>
                        <Icon
                            name="play"
                            color="#fff"
                            size={16}
                            style={styles.icon}/>
                        <Text style={styles.play}>PLAY</Text>
                    </TouchableOpacity>
                    <View style={styles.viewTitle}>
                        <View style={styles.tit}>
                            <View>
                                <Text style={styles.titleTop}>{this.state.items.title}</Text>
                                <Text style={styles.year}>({this.state.items.year})</Text>
                            </View>
                            <View style={styles.staranting}>
                                <I
                                    name="star"
                                    color="#fff"
                                    size={25}
                                    style={styles.star}
                                />
                                <I
                                    name="star"
                                    color="#fff"
                                    size={25}
                                    style={styles.star}

                                />
                                <I
                                    name="star"
                                    color="#fff"
                                    size={25}
                                    style={styles.star}

                                />
                                <I
                                    name="star-outlined"
                                    color="#fff"
                                    size={25}
                                    style={styles.star}

                                />
                                <I
                                    name="star-outlined"
                                    color="#fff"
                                    size={25}
                                    style={styles.star}
                                />
                                <Text style={styles.rating}>{this.state.items.rating}</Text>
                            </View>
                        </View>
                        <Text style={styles.title}>{this.state.items.description_intro}</Text>
                    </View>
                </View>
                
                
            </View>
        );
    }
}

//--------------------------------------------------------------------------------------------------------------------//
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#222222',
        flex: 1,
    },
    itemContainer: {
        flex: 1,
    },
    button:{
        flexDirection: 'row',
        backgroundColor: '#A21216',
        marginLeft: '5%',
        marginRight: '5%',
        padding: '3%',
        borderRadius: 50,
        justifyContent: 'center',    
    },
    itemImage: {
        top: '25%',
        left: '5%',
        width: '30%',
        height: '115%',
        position: 'absolute',
        borderRadius: 5,
    },
    itemBackground: {
        width: '100%',
        height: 180,
    },
    title:{
        color: 'white',
        textAlign: 'justify',
        marginTop: '2%',
    },
    text:{
        paddingTop: '7%',
    },
    icon:{
        marginTop: 2,
        marginRight: 10,
    },
    play:{
        color: 'white',
    },
    top:{
        marginLeft: '45%',
        marginTop: '5%',
        flexDirection: 'row',
    },
    like:{
        alignItems: 'center',
        marginRight: '20%',
    },
    viewTitle:{
        marginLeft: '5%',
        marginRight: '5%',
    },
    titleTop:{
        fontSize: 25,
        color: 'white',
        fontFamily: 'arial',
        marginTop: '2%',
        marginRight: '12%',
    },
    tit:{
    },
    star:{
    },  
    rating:{
        marginRight: '1%',
        fontSize: 15,
        color: 'white',
        fontFamily: 'arial',
        marginBottom: '2%',
        marginLeft: '3%',
    },
    staranting:{
        flexDirection: 'row',
    },
    year:{
        fontSize: 25,
        color: 'white',
        fontFamily: 'arial',
        marginBottom: '2%',
        marginRight: '12%',
    },
});
