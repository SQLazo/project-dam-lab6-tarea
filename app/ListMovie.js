//--------------------------------------------------------------------------------------------------------------------//
import React, { Component } from 'react';
import {
    View, 
    FlatList, 
    StyleSheet, 
    Text, 
    Image,
    TouchableOpacity
} from 'react-native';

//--------------------------------------------------------------------------------------------------------------------//
import Icon from "react-native-vector-icons/Ionicons";

//--------------------------------------------------------------------------------------------------------------------//
function Item({id, title, image, summary, navigation}) {
    return(
        <TouchableOpacity  
            style={{borderBottomWidth: 1}}
            onPress={() => navigation.navigate('Details of Movie',{
                itemId: id,
            })}
            >
          <View style={styles.itemContainer}>
            <View style={styles.thumbContainer}>
              <Image source={{uri: image}} style={styles.itemImage}
                resizeMode="cover"
              />
            </View>
            <View style={styles.description}>
                <Text style={styles.itemName}>{title}</Text>
                <Text numberOfLines={2} style={styles.itemSubtitle}>{summary}</Text>
            </View>
            <Icon
                name="ios-arrow-forward"
                color="#fff"
                size={20}
                style={styles.icon}
            />
          </View>
        </TouchableOpacity>
    );
}

//--------------------------------------------------------------------------------------------------------------------//
export default class ConexionFetch extends Component{
    constructor(props){
        super(props);

        this.state = {
            textValue: 0,
            count: 0,
            items: [],
            error: null,
        };
    }
    async componentDidMount() {
        await fetch('https://yts.mx/api/v2/list_movies.json')
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        items: result.data.movies,
                    });
                },
                error => {
                    this.setState({
                        error: error,
                    });
                },
            );
    }

    render(){
        return(
            <View style={styles.container}>
                <FlatList
                    data={this.state.items.length > 0 ? this.state.items : []}
                    renderItem={({item}) => (
                        <Item
                            id={item.id}  
                            title={item.title} 
                            image={item.medium_cover_image} 
                            summary={item.summary}
                            navigation={this.props.navigation}
                        />
                    )}
                    keyExtractor={item => item.id}
                />

            </View>
        );
    }
}

//--------------------------------------------------------------------------------------------------------------------//
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#222222',
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        justifyContent: 'flex-start',
        margin: 15,
    },
    itemName: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
    },
    itemSubtitle: {
        fontSize: 12,
        color: 'gray',
        textAlign: 'justify',
        color: 'white',
    },
    description: {
        flex: 1,
        alignItems: 'stretch',
        marginLeft: 20,
        paddingRight: 10,
    },
    itemImage: {
        width: 70,
        height: 70,
        borderRadius: 50,
    },
    icon: {
        marginTop: 30,
        paddingRight: 10,
        paddingLeft: 5,
    },
});
