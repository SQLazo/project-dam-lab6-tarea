//--------------------------------------------------------------------------------------------------------------------//
import React, {Component} from 'react';
import {Text, View} from  'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

//--------------------------------------------------------------------------------------------------------------------//
import LoginScreen from './app/Login';
import ListScreen from './app/ListMovie';
import DetailScreen from './app/DetailMovie';
import VideoScreen from './app/Video';
import SettingsScreen from './app/Settings';

//--------------------------------------------------------------------------------------------------------------------//
const Stack = createStackNavigator();
//const Tab = createMaterialBottomTabNavigator();
const Tab = createBottomTabNavigator();

//--------------------------------------------------------------------------------------------------------------------//
const TabView = () => {
  return(
    <Tab.Navigator>
      <Tab.Screen 
        name='List of Movies' 
        component={ListScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}/>
      <Tab.Screen 
        name='Video' 
        component={VideoScreen} 
        options={{
          tabBarLabel: 'Video',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="video" color={color} size={size} />
          ),
        }}/>
      <Tab.Screen 
        name='Settings' 
        component={SettingsScreen}
        options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="settings" color={color} size={size} />
          ),
        }}/>
    </Tab.Navigator>
  )
}

//--------------------------------------------------------------------------------------------------------------------//
export default App = () =>  {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false }}/>
          <Stack.Screen name="Home" component={TabView} options={{headerShown: false }}/>
          <Stack.Screen name="Details of Movie" component={DetailScreen} 
            options={{ 
              headerStyle: {
                backgroundColor: '#A21216',
              },
              headerTintColor: '#fff',
            }} 
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
}
